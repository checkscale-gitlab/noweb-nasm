ARG CI_REGISTRY
FROM ${CI_REGISTRY}/sw4j-net/noweb/master:latest

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install build-essential make nasm
